package answer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Answer {
    
    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;
    
    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }
    
    public List<Integer> getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }
    
    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }
    
    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(filePath.getFileName().toString());
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        List<String> answerImported = Arrays.asList(reader.readLine().split(""));
        return answerImported.stream().map(Integer::parseInt).collect(Collectors.toList());
    }
    
    private List<Integer> generateRandomAnswer() {
        List<Integer> randomNumbers = new ArrayList<>();
        int iterator = 0;
        while (iterator < ANSWER_LENGTH) {
            int randomNumber = (int) (Math.random() * ANSWER_NUMBER_LIMIT);
            while (randomNumbers.contains(randomNumber)) {
                randomNumber = (int) (Math.random() * ANSWER_NUMBER_LIMIT);
            }
            randomNumbers.add(randomNumber);
            iterator++;
        }
        return randomNumbers;
    }
    
    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        if (answer.stream().distinct().count() != 4) {
            throw new InvalidAnswerException("Answer is not valid");
        }
    }
}
