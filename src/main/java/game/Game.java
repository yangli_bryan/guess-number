package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Game {
    
    private Answer answer;
    private Map<String, String> guessResult;
    
    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }
    
    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }
    
    public boolean isOver() {
        return guessResult.values().contains("4A0B") || guessResult.size() == 6;
    }
    
    public String getResult() {
        StringBuilder resultToPrint = new StringBuilder();
        for (Map.Entry<String, String> unitGame: guessResult.entrySet()) {
            resultToPrint.append(unitGame.getKey() + " "+ unitGame.getValue() + "\n");
        }
        if (guessResult.values().contains("4A0B")) {
            resultToPrint.append("Congratulations, you win!");
        } else if (guessResult.size() == 6) {
            resultToPrint.append("Unfortunately, you have no chance, the answer is " + answer.toString() + "!");
        }
        return resultToPrint.toString();
    }
    
    private int getBSize(List<Integer> numbers) {
        return numbers.stream().reduce(0, (count, value) ->
                this.answer.getAnswer().contains(value) ? count = count + 1 : count) - getASize(numbers);
    }
    
    private int getASize(List<Integer> numbers) {
        return numbers.stream().reduce(0, (count, value) ->
                this.answer.getAnswer().indexOf(value) == numbers.indexOf(value) ? count = count + 1 : count);
    }
}
