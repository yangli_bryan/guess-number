package game;

import java.util.ArrayList;
import java.util.Map;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;
    
    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }
    
    
    public String getResult() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<String, String> entry: guessRecord.entrySet()) {
            String unitResult = entry.getValue();
            result.append(entry.getKey() + " " + entry.getValue() + "\n");
        }
        result.delete(result.lastIndexOf("\n"), result.length());
        return result.toString();
    }
    
    public GameResult getGameResult() {
        ArrayList<String> gameResults = new ArrayList<>(guessRecord.values());
        if (gameResults.contains("4A0B")){
            return GameResult.WIN;
        } else if (guessRecord.size() < 6) {
            return GameResult.NORMAL;
        }
        return GameResult.LOST;
    }
}
